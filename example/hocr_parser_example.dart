import 'dart:io' as io;
import 'package:args/args.dart';
import 'package:hocr_parser/src/hocr_doc.dart';

void main(List<String> arguments) {
  late final String? iFile;
  late final String? oFile;

  final parser = ArgParser()
    ..addOption('ifile', abbr: 'i', callback: (fname) => iFile = fname)
    ..addOption('ofile', abbr: 'o', callback: (fname) => oFile = fname);
  parser.parse(arguments);

  try {
    print("HOCR Parser");
    if (iFile == null) {
      throw Exception("Specify input file (.hocr)");
    }
    if (oFile == null) {
      throw Exception("Specify output file (.json)");
    }
    if (iFile != null && oFile != null) {}
    print("\tLoading $iFile");
    final xmlString = io.File(iFile!).readAsStringSync();
    print("\tParsing $iFile");
    final HOCRDoc hocrDoc = HOCRDoc.fromXML(xmlString: xmlString);
    print("\tWriting $oFile");
    io.File(oFile!).writeAsStringSync(hocrDoc.toJson());
    print("Done");
  } catch (e) {
    print("\t${e.toString()}");
    print("Done with Error");
  }
}
