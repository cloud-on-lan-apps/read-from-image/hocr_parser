// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

import 'package:xml/xml.dart';

class HOCRNode {
  static const hocrBlocks = [
    "ocr_page",
    "ocr_carea",
    "ocr_par",
    "ocr_line",
    "ocrx_word",
    'ocr_textfloat',
    'ocr_header',
    'ocr_photo',
    'ocr_separator',
    'ocr_caption'
    //"ocrp_wconf",
  ];
  String htmlTag;
  String parentID;
  List<String> children;
  Map<String, String> attributes;
  String? text;
  Map<String, dynamic> properties;
  HOCRNode(
      {required this.htmlTag,
      required this.parentID,
      required this.children,
      required this.attributes,
      required this.text,
      required this.properties});

  factory HOCRNode.fromXML(XmlElement xmlElement) {
    final children = xmlElement.children
        .where((p0) => isHOCRBlock(p0))
        .map((p0) => p0.getAttribute('id') ?? "Error: HOCR FAIL")
        .toList();

    return HOCRNode(
        htmlTag: xmlElement.name.toString(),
        attributes: {
          for (var e in xmlElement.attributes.where(
            (p0) => p0.name.toString() != "title",
          ))
            (e).name.toString(): (e).value
        },
        properties: extractProperties(xmlElement.getAttribute('title')),
        parentID: xmlElement.parent?.getAttribute('id') ?? 'root',
        children: children,
        text: children.isEmpty ? xmlElement.text : null);
  }

  static Map<String, dynamic> extractProperties(String? title) {
    Map<String, dynamic> properties = {};
    if (title == null) return {};

    List<String> propertiesRaw = title.split(';');
    for (String propertyRaw in propertiesRaw) {
      List<String> tmp = propertyRaw.trim().split(' ');
      String key = tmp[0].trim();
      List<String> values = tmp.sublist(1);

      switch (key) {
        case 'scan_res':
        case 'bbox':
        case 'x_wconf':
        case 'baseline':
        case "x_size":
        case "x_descenders":
        case "x_ascenders":
          properties[key] = values.length == 1
              ? double.parse(values[0])
              : values
                  .map(
                    (e) => double.parse(e),
                  )
                  .toList();

          break;
        case 'ppageno':
          properties[key] = values.length == 1
              ? int.parse(values[0])
              : values
                  .map(
                    (e) => int.parse(e),
                  )
                  .toList();

          break;
        case 'image':
          properties[key] = (values.length == 1) ? values[0] : values;
          break;
        default:
          print("$key $values");
      }
    }

    return properties;
  }

  static bool isHOCRBlock(p0) => hocrBlocks.contains(p0.getAttribute('class'));

  @override
  String toString() {
    return 'HOCRNode('
        'attributes: $attributes,'
        '${properties.isEmpty ? " properties: $properties," : ""}'
        ' parentID: $parentID'
        ' tag: $htmlTag'
        '${children.isNotEmpty ? ", children: $children" : ""}'
        '${text != null ? ", text: $text" : ""})';
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'htmlTag': htmlTag,
      'parentID': parentID,
      'children': children,
      'attributes': attributes,
      'text': text,
      'properties': properties.isEmpty ? null : properties
    };
  }

  String toJson() => json.encode(toMap());
}
