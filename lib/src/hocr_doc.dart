import 'dart:convert';

import 'package:xml/xml.dart';

import 'hocr_node.dart';

class HOCRDoc {
  List<HOCRNode> nodes;
  HOCRDoc({
    required this.nodes,
  });

  factory HOCRDoc.fromXML({required String xmlString}) {
    final XmlDocument xmlDocument = XmlDocument.parse(xmlString);
    final body = xmlDocument.findAllElements('body').toList()[0];

    final list = body.children
        .where((p0) => HOCRNode.isHOCRBlock(p0))
        .map((e) => e as XmlElement)
        .toList();

    List<HOCRNode> descendants = [];
    List<HOCRNode> children = [];
    if (list.isNotEmpty) {
      for (final e in list) {
        final childAndDescendants =
            getAll(xmlElement: e, parent: body.getAttribute('id') ?? 'root');
        children.add(childAndDescendants[0]);
        descendants.addAll(childAndDescendants);
      }
    }

    return HOCRDoc(nodes: descendants);
  }

  static List<HOCRNode> getAll(
      {required XmlElement xmlElement, required String parent}) {
    final list = xmlElement.children
        .where((p0) => HOCRNode.isHOCRBlock(p0))
        .map((e) => e as XmlElement)
        .toList();

    List<HOCRNode> descendants = [];
    List<HOCRNode> children = [];
    if (list.isNotEmpty) {
      for (final e in list) {
        final childAndDescendants = getAll(
            xmlElement: e, parent: xmlElement.getAttribute('id') ?? 'root');
        children.add(childAndDescendants[0]);
        descendants.addAll(childAndDescendants);
      }
    }
    return [HOCRNode.fromXML(xmlElement), ...descendants];
  }

  @override
  String toString() => 'HOCRNodes(nodes: $nodes)';

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'nodes': nodes.map((x) => x.toMap()).toList(),
    };
  }

  String toJson() => json.encode(toMap());
}
